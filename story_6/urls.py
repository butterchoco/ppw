from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import index, profile

urlpatterns = [
    url(r'^beranda/', index),
    url(r'^$', RedirectView.as_view(url='beranda/')),
    url(r'profile/', profile),
]